<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request as GuzzleRequest;

class ChuckController extends Controller
{
    protected $chuck_url = 'https://api.chucknorris.io/jokes/random';
    protected $number_of_jokes = 25;

    public function index(){

        $jokes = [];
        $used_ids = [];
        $joke_count = 0;

        while($joke_count < $this->number_of_jokes)
        {
            $r = Http::get($this->chuck_url);

            if ($r->failed()) continue;

            if (array_search($r->json('id'), $used_ids) === false) {
                $used_ids[] = $r->json('id');
                $jokes[] = $r->json();
                $joke_count ++;
            }
        }

        return response()->json($jokes);

    }

    public function index_async(){

        $jokes = [];
        $used_ids = [];
        $client = new Client();
        $r = new GuzzleRequest('GET',$this->chuck_url);
        $requests = array_fill(0,$this->number_of_jokes,$r);

        while(count($requests))
        {
            $respuestas = Pool::batch($client, $requests, [
                'concurrency' => 5,
            ]);

            foreach($respuestas as $r){

                #var_dump($r);
                $status = $r->getStatusCode();
                
                if ($status< 200 && $status >= 300) continue;
                
                $dec_json = json_decode($r->getBody()->getContents());

                if (array_search($dec_json->id, $used_ids) === false) {
                    $jokes[] = $dec_json;
                    array_pop($requests);
                }
            }
        }

        return $jokes;

    }
}
