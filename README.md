
# Ejercicio con Laravel - API Rest (Laravel 9)
En este ejercicio se consume una API con los hechos de Chuck Norris usando el siguiente endpoint que nos devuelve un hecho aleatorio:
```
https://api.chucknorris.io/jokes/random
```
El objetivo es obtener 25 hechos diferentes y devolverlos en un endpoint.
## Soluciones
Incorporé 2 soluciones. . .
### La solución básica
```
GET /api/chuck-facts
```
Una muy básica donde se realiza petición tras petición y se comprueba si ya existe el id en un arreglo de soluciones. Lo malo es que como debe esperar por cada petición es bastante lenta.
### La solución asíncrona (y mejor)
```
GET /api/chuck-facts-async
```
Usando la librería de **GuzzleHttp** por fuera de los handlers de Laravel (que de por sí ya la usan *pero están pobremente documentados a mi parecer*) y una pool de peticiones. Se prefiere usar el método **batch** ya que iniciando varias peticiones de forma asíncrona una tras otra nos trae problemas de condición de carrera, por ello se vé en el código que lleno un arreglo con la misma petición para iniciarla y hacer una pool con un **batch** de peticiones.
### Se tiene en cuenta que ...
No programé algo para "parar" o dejar de realizar las peticiones en caso de muchos fallos, pues ambos bloques de código donde se reallizan las peticiones están dentro de un while lo que podría, en caso de problemas de conexión, ejecutar el código hasta el límite de ejecución de PHP.
